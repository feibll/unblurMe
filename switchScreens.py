#!/usr/bin/env python3

import subprocess
import json

workspacesJson = subprocess.run(["i3-msg","-t", "get_workspaces"],
        stdout=subprocess.PIPE,
        universal_newlines=True)

workspaces = json.loads(workspacesJson.stdout)

initialScreen = -1

for index, item in enumerate(workspaces):
    if item["focused"]:
        initialScreen = "alt+" + str(item["num"])
        break

if initialScreen == -1:
    print("failure! Exit with -1")
    exit(-1)

print("Initial screen: %s" % initialScreen)
screen5 = "alt+3"
screen6 = "alt+4"

for i in range(0,2):
    subprocess.run(["xdotool","key", screen5])
    subprocess.run(["sleep","1"])
    subprocess.run(["xdotool","key", screen6])
    subprocess.run(["sleep","1"])

subprocess.run(["xdotool","key", initialScreen])
